#include "board.h"
#include <fstream>
#include <iostream>
#include <math.h>
using namespace std;

int abs(int x)
{
	if(x<0)
		return -x;
	return x;
}

Board::Board() // default constructor. Reads the board from FNAME
{
	ifstream in(FNAME);
	char input;
	T1_X=-1;		// Teleporter x and y coord
	T1_Y=-1;
	T2_X=-1;
	T2_Y=-1;
	has_tele=false;

	cell = new signed char*[SIZE];
	// search for the teleporter locations
	for(int i=0; i<SIZE; ++i)
	{
		cell[i] = new signed char[SIZE];
		for(int j=0; j<SIZE; ++j)
		{
			in >> input;
			if(input=='.')
				cell[i][j]=FREE;
			else if(input=='W')
				cell[i][j]=WATER;
			else if(input=='R')
				cell[i][j]=ROCK;
			else if(input=='B')
				cell[i][j]=BARRIER;
			else if(input=='T')
				cell[i][j]=TELEPORT;
			else if(input=='L')
				cell[i][j]=LAVA;
			else cout << "READ ERROR: character \'" << input << "\' is unexpected." << endl;

			if(cell[i][j]==TELEPORT)
			{
				if(T1_X==-1)
				{
					T1_X=i;
					T1_Y=j;
				}
				else if(T2_X==-1)
				{
					T2_X=i;
					T2_Y=j;
					has_tele=true;
				}
			}
		}
	}
	in.close();
	for(int i=0; i<BYTES; ++i)
	{
		visited_[i]=0; // initially, none are visited
	}
}
Board::Board(Board *b) // copy constructor. Replicates the data on a new board
{
	/*for(int i=0; i<SIZE; ++i)
	{
		for(int j=0; j<SIZE; ++j)
		{
			cell[i][j] = b->cell[i][j];
		}
	}*/
	cell=b->cell;
	for(int i=0; i<BYTES; ++i)
	{
		visited_[i]=b->visited_[i];
	}
	T1_X = b->T1_X;
	T1_Y = b->T1_Y;
	T2_X = b->T2_X;
	T2_Y = b->T2_Y;
	has_tele = b->has_tele;
}
int Board::step(int &x, int &y) // step on a square. Changes the state of that square to VISITED. Returns the cost of the step
{
	if(x<0 || y<0 || x>=SIZE || y>=SIZE)
	{
		cout << "Coordinates ("<< x << ", " << y <<") are out of bounds. Cannot step there.\n";
		return 0;
	}
	if(blocked(x,y))
	{
		cout << "Coordinates ("<< x << ", " << y <<") aren't traversible. Cannot step there.\n";
		return 0;
	}
	if(cell[x][y]==TELEPORT)
	{
		setVisited(x,y);
		if(x==T1_X && y==T1_Y)
		{
			setVisited(T2_X,T2_Y);	// mark other teleporter as VISITED
			x=T2_X;						// update coordinates
			y=T2_Y;
		}
		else if(x==T2_X && y==T2_Y)
		{
			setVisited(T1_X,T1_Y);	// mark other teleporter as VISITED
			x=T1_X;						// update coordinates
			y=T1_Y;
		}
		has_tele=false;					// the teleport squares may not be revisited
		return 1; // move onto teleporter costs 1 step
	}
	else
	{
		int val = cell[x][y];
		setVisited(x,y);
		return val;
	}
	
}
int Board::heuristic_euclidean(int start_x, int start_y, int goal_x, int goal_y)
{
	return (int)( sqrt((goal_x-start_x)*(goal_x-start_x)+(goal_y-start_y)*(goal_y-start_y)) *100) ;// / sqrt(5)); // I multiplied the traditional number-of-moves heuristic by 100 sqrt(5) to have better precision when truncating to int
}
int Board::heuristic_manhattan_sans_tele(int start_x, int start_y, int goal_x, int goal_y)
{	
	int x_contrib = abs(goal_x-start_x);
	int y_contrib = abs(goal_y-start_y);
	if(y_contrib >= x_contrib*2)				// for this case, the motion could be always mainly in y 
		return y_contrib/2;
	if (x_contrib >= y_contrib*2)				// for this case, the motion could be always mainly in x
		return x_contrib/2;
	if (y_contrib > x_contrib)
		return (y_contrib-x_contrib)/2+(x_contrib*2)/3; // for this case, part the motion could be always mainly in x, the rest would alternate between mainly in x and mainly in y
	return (x_contrib-y_contrib)/2+(y_contrib*2)/3;		// for this case, part the motion could be always mainly in y, the rest would alternate between mainly in x and mainly in y
}
int Board::heuristic_manhattan_tele(int start_x, int start_y, int goal_x, int goal_y)
{
	int h1 = heuristic_manhattan_sans_tele(start_x, start_y, goal_x, goal_y);
	if(!has_tele)
		return h1;
	int h2 = heuristic_manhattan_sans_tele(start_x, start_y, T1_X, T1_Y)
			+heuristic_manhattan_sans_tele(T2_X, T2_Y, goal_x, goal_y);
	int h3 = heuristic_manhattan_sans_tele(start_x, start_y, T2_X, T2_Y)
			+heuristic_manhattan_sans_tele(T1_X, T1_Y, goal_x, goal_y);
			
	// account for possible teleportation by return the smallest of the three
	if(h1<h2 && h1<h3)
		return h1;
	if(h2<h3)
		return h2;
	return h3;
	
}
int Board::heuristic_warnsdorff(int start_x, int start_y)
{
	int h1=0;
	for(int i=0; i<NUM_NEIGHBORS; ++i)
	{
		if(can_move(start_x, start_y, i))
			++h1;
	}
	return h1;
}

bool Board::can_move(int from_x, int from_y, int move_index)
{
	int x = from_x + neighbor_x[move_index];		// coordinates of the child node
	int y = from_y + neighbor_y[move_index];
	int jx1 = from_x + jump1_x[move_index];		// coordinates of jump square 1
	int jy1 = from_y + jump1_y[move_index];
	int jx2 = from_x + jump2_x[move_index];		// coordinates of jump square 2
	int jy2 = from_y + jump2_y[move_index];
	return
		(x>=0 && x<SIZE && y>=0 && y<SIZE && !blocked(x,y)) 		//test for edges and landing square
		&& (cell[jx1][jy1] != BARRIER || cell[jx2][jy2] != BARRIER);	//test for jumping across barrier
			
}

void Board::draw(int start_x, int start_y)
{
	char board[SIZE*(SIZE+1)*2];
	int k=0;
	for(int i=0; i<SIZE; ++i)
	{
		for(int j=0; j<SIZE; ++j)
		{
			if(getVisited(i,j))
				board[k++] = '*';
			else if(cell[i][j]==FREE)
				board[k++] = '.';
			else if(cell[i][j]==WATER)
				board[k++] = 'W';
			else if(cell[i][j]==ROCK)
				board[k++] = 'R';
			else if(cell[i][j]==BARRIER)
				board[k++] = 'B';
			else if(cell[i][j]==TELEPORT)
				board[k++] = 'T';
			else if(cell[i][j]==LAVA)
				board[k++] = 'L';
			else
				board[k++] = '?';
				
			if(i==start_x && j==start_y)		// mark the starting point
				board[k++] = 'S';
			else if(start_x >=0 && i==GOAL_X && j==GOAL_Y)		// mark the goal. May use start_x=-1 to omit start and goal markers
				board[k++] = 'G';
			else
				board[k++] = ' ';
		}
		board[k++] ='\n';
	}
	board[k++] = 0;
	printf(board);
	
}

int Board::get(int i, int j)
{
	return cell[i][j];
}
bool Board::blocked(int i, int j)
{
	return cell[i][j]<0 || getVisited(i,j);
}
bool Board::getVisited(int i, int j)
{
	i=i*SIZE+j;
	int bitnum = i%BITS_PER_CHAR;
	return visited_[i/BITS_PER_CHAR] & (1 << bitnum);
}
void Board::setVisited(int i, int j)
{
	i=i*SIZE+j;
	int bitnum = i%(BITS_PER_CHAR);
	visited_[i/BITS_PER_CHAR] |= (1 << bitnum);
}
