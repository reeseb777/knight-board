#ifndef NODE_H
#define NODE_H

#include "board.h"

class Node
{
public:
	Node(int start_x, int start_y); // Constructor generates the starting Node
	Node(Node *my_parent, int my_x, int my_y, Node **top, bool alternate=false); // Constructor generates a child Node
	bool atGoal();
	bool goal_reachable();									// return true if goal is not (immediately) blocked off
	Node *prev;		// Node representing previous in sorted queue. Null for first node
	Node *next;		// Node representing next in sorted queue. Null for last node
	int x;	// the current position on the Board
	int y;
	Board *b; // current state of the Board
	int f; // the cost to reach this Node
	int h; // the heuristic (a lower bound on cost) to reach the END
	Node *parent;	// Node representing previous step. Null for starting node
private:

	void insert_sorted(Node **top); // place this node into the sorted queue (ascending sort by f+h)
};

#endif
