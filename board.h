#ifndef BOARD_H
#define BOARD_H

/*
Board class represents the instantaneous state of a knight board

*/

#include "problem_setup.h"

/*
Codes for the various cell states
	unavailable states are conveniently valued < 0
	free, water, and lava states are conveniently valued at their integer costs
	*** Assumption: exactly two or zero Teleport squares exist
*/
#define FREE 1
//#define VISITED (-1)
#define WATER 2
#define ROCK (-2)
#define BARRIER (-3)
#define LAVA 5
#define TELEPORT 0

const int BITS_PER_CHAR = (8*sizeof(char));
const int BYTES=(SIZE*SIZE+7)/BITS_PER_CHAR;

int abs(int x);

class Board
{
public:
	Board(); // default constructor. Reads the board from FNAME
	Board(Board *b); // copy constructor. Replicates the data on a new board
	int step(int &x, int &y); // step on a square. Changes the state of that square to VISITED. Returns the cost of the step
	int heuristic_euclidean(int start_x, int start_y, int goal_x, int goal_y); // Euclidean distance heuristic
	int heuristic_manhattan_sans_tele(int start_x, int start_y, int goal_x, int goal_y); // Tweaked Manhattan distance heuristic
	int heuristic_manhattan_tele(int start_x, int start_y, int goal_x, int goal_y);	// Tweaked Manhattan distance heuristic accounting for teleport paths
	int heuristic_warnsdorff(int start_x, int start_y);	// Warnsdorff heuristic - favors cells from which there are fewer available moves
	bool can_move(int from_x, int from_y, int move_index);	// can the knight move to one of 8 predefined relative positions?
															//(positions for each index are listed in problem_setup.h) 
	void draw(int start_x=-1, int start_y=-1);
	int get(int i, int j);
	bool blocked(int i, int j);
	bool getVisited(int i, int j);
	void setVisited(int i, int j);
private:
	signed char **cell;			// data representing the board is encoded with integers as above defined
	char visited_[BYTES];		// bit storage for visited/non-visited
	int T1_X;		// Teleporter x and y coord
	int T1_Y;
	int T2_X;
	int T2_Y;
	bool has_tele;	// does this board have a teleporter? (Set this to false after teleporter has been traversed)
};


#endif
