#ifndef PROBLEM_SETUP_H
#define PROBLEM_SETUP_H

/*******************************************************/
/*    Modifiable options: edit these at will           */
/*******************************************************/

#ifndef PROBLEM_LEVEL
#define PROBLEM_LEVEL 5
#endif

#if PROBLEM_LEVEL == 1			// problem one: validity check of a path
//#define FNAMEP "testpath.txt"	// FNAMEP specifies the filename to open and read the path. Specify "@USER" to get the path from the user
#define FNAMEP "result_887_moves_1098_steps.txt"	// FNAMEP specifies the filename to open and read the path. Specify "@USER" to get the path from the user
#define FNAME "32by32.txt"		// FNAME specifies the text file representing the board
#define SIZE 32					// side length of the SQUARE board. Should match file contents of FNAME
//#define FNAMEP "@USER"
//#define FNAME "8by8.txt"		// FNAME specifies the text file representing the board
//#define SIZE 8					// side length of the SQUARE board. Should match file contents of FNAME
#define ANIMATE_SKIP 1 			// animate by displaying every ANIMATE_SKIP iterations of the loop

#elif PROBLEM_LEVEL == 2		// problem two: pathing from START to GOAL 8 by 8 (solution identical to problem three)
#define FNAME "8by8.txt"		// FNAME specifies the text file representing the board
#define SIZE 8					// side length of the SQUARE board. Should match file contents of FNAME
#define ANIMATE_SKIP 1 			// animate by displaying every ANIMATE_SKIP iterations of the loop

#elif PROBLEM_LEVEL == 3		// problem three: optimal pathing from START to GOAL 8 by 8
#define FNAME "8by8.txt"		// FNAME specifies the text file representing the board
#define SIZE 8					// side length of the SQUARE board. Should match file contents of FNAME
#define ANIMATE_SKIP 1 			// animate by displaying every ANIMATE_SKIP iterations of the loop

#elif PROBLEM_LEVEL == 4		// problem four: optimal pathing from START to GOAL 32 by 32
#define FNAME "32by32.txt"		// FNAME specifies the text file representing the board
#define SIZE 32					// side length of the SQUARE board. Should match file contents of FNAME
#define ANIMATE_SKIP 100 		// animate by displaying every ANIMATE_SKIP iterations of the loop

#elif PROBLEM_LEVEL == 5		// problem three: Knight's tour START to GOAL 32 by 32
//#define FNAME "8by8.txt"		// FNAME specifies the text file representing the board
//#define SIZE 8				// side length of the SQUARE board. Should match file contents of FNAME
//#define FNAME "32by32blank.txt"
#define FNAME "32by32.txt"		// FNAME specifies the text file representing the board
#define SIZE 32					// side length of the SQUARE board. Should match file contents of FNAME
#define ANIMATE_SKIP 100		// animate by displaying every ANIMATE_SKIP iterations of the loop
//#define LONG_WAY

#define SORT_OPTION OPT_WT_A		// Pick the sorting heuristic option (PROBLEM_LEVEL 5 only)
#define OPT_W_E 1				// sort potential moves by Warnsdorff heuristic, then by Euclidean heuristic
#define OPT_E_W 2				// sort potential moves by Euclidean heuristic, then by Warnsdorff heuristic
#define OPT_WT 3				// sort potential moves by Warnsdorff heuristic, Euclidean heuristic weighted sum
#define OPT_W_A 4				// sort potential moves by Warnsdorff heuristic, then by A* Steps heuristic
#define OPT_WT_A 5				// sort potential moves by Warnsdorff heuristic, A* Steps heuristic weighted sum (this one works the best! try using command line "weighting" parameter 1 or 2)
#define OPT_W_W 6				// sort potential moves by Warnsdorff heuristic, then by A* Steps heuristic
#define OPT_WT_W 7				// sort potential moves by Warnsdorff heuristic, A* Steps heuristic weighted sum

#endif

#define ANIMATE 1		 	// turn the intermediate printouts ON(1) or OFF(0)

// Start and End point selection. Use these for PROBLEM_LEVEL 2 onwards
#if PROBLEM_LEVEL == 4
const int START_X=3;			// X is the vertical axis
const int START_Y=15;			// Y is the horizontal axis
const int GOAL_X=0;
const int GOAL_Y=0;
#elif PROBLEM_LEVEL == 5
const int START_X=1;			// X is the vertical axis
const int START_Y=4;			// Y is the horizontal axis
const int GOAL_X=4;
const int GOAL_Y=31;
#else
const int START_X=1;			// X is the vertical axis
const int START_Y=4;			// Y is the horizontal axis
const int GOAL_X=SIZE-1;
const int GOAL_Y=0;
#endif
/*******************************************************/
/*    END of modifiable options                        */
/*******************************************************/

#define NULL 0

const int neighbor_x[] = {-1, 1, 2, 2, 1,-1,-2,-2};	// relative coordinates of potential knight moves
const int neighbor_y[] = {-2,-2,-1, 1, 2, 2, 1,-1};
const int jump1_x[]    = { 0, 0, 1, 1, 0, 0,-1,-1};	// corresponding relative coordinates of jump squares
const int jump1_y[]    = {-1,-1, 0, 0, 1, 1, 0, 0};
const int jump2_x[]    = {-1, 1, 1, 1, 1,-1,-1,-1};
const int jump2_y[]    = {-1,-1,-1, 1, 1, 1, 1,-1};
const int NUM_NEIGHBORS=8;


#endif
