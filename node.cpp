#include "node.h"
#include <cmath>

Node::Node(int start_x, int start_y) // Constructor generates the starting Node
{
	x=start_x;
	y=start_y;
	b=new Board();
	b->setVisited(x,y);
	f=0;
	h=b->heuristic_manhattan_tele(start_x, start_y, GOAL_X, GOAL_Y);
	parent=next=prev=NULL;
}
Node::Node(Node *my_parent, int my_x, int my_y, Node **top, bool alternate) // Constructor generates a child Node
{
	b=new Board(my_parent->b);								// making a new Board based on a duplicate of the parent's Board
															// one the function b->step() is called, the board state is updated
	if(!alternate)
	{
		f=my_parent->f + b->step(my_x, my_y);								// use the cost and heuristics for the A* search
		h=b->heuristic_manhattan_tele(my_x, my_y, GOAL_X, GOAL_Y);
		x=my_x; // these should be assigned after the call to b->step() since the function may modify my_x, my_y (teleport) 
		y=my_y;
	}
	else
	{
		b->step(my_x, my_y);												// use the heuristics for the recursive search (PROBLEM_LEVEL 5)
		f=b->heuristic_euclidean(my_x, my_y, GOAL_X, GOAL_Y);
		h=b->heuristic_warnsdorff(my_x, my_y);
		x=my_x;
		y=my_y;
		if(atGoal())
			f=9999;
	}
	parent=my_parent;
	if(top!=NULL)
		insert_sorted(top);	// puts the node in sorted queue for A*(next and prev are initialized)
							// this step may be ignored (i.e. top=NULL) for the recursive search,
							// which does its own sorting in main.cpp function solve_tour() 
}
bool Node::atGoal()
{
	return x==GOAL_X && y==GOAL_Y;
}
bool Node::goal_reachable()
{
	for(int i=0; i<NUM_NEIGHBORS; ++i)
	{
		int pt_x = GOAL_X + neighbor_x[i], pt_y = GOAL_Y + neighbor_y[i];
		if(pt_x >= 0 && pt_x < SIZE && pt_y >= 0 && pt_y < SIZE && !b->blocked(pt_x,pt_y))
			return true;
		if(pt_x==x && pt_y == y)
			return true;
	}
	return false;
}
void Node::insert_sorted(Node **top) // inserts this Node into a linked list, maintaining the ascending sort by f+h
{
	if(*top==NULL)			// place this node as the only element the list
	{
		*top=this;
		this->next=NULL;
		this->prev=NULL;
		return;
	}
	Node *temp=*top;

	while(temp->f+temp->h < f+h)
	{
		if(temp->next == NULL) // place this Node at the end of the list
		{
			temp->next = this;
			this->prev = temp;
			next = NULL;
			return;
		}
		temp = temp->next;
	}

	// place the node immediately before temp
	this->prev = temp->prev;
	if(temp->prev == NULL)
		*top=this;
	else
		temp->prev->next = this;
	temp->prev = this;
	this->next = temp;
}
