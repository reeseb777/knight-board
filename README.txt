This is a solver for the Knight Board programming challenge issued by Cruise Automation
(code instructions and problem description included below)
Author: Brandon Reese

Instructions
------------
To use the solver, modify the following MACRO values and constants in problem_setup.h to solve the problem you want
START_X, START_Y // integer starting position (note y is the horizontal axis in display function)
GOAL_X, GOAL_Y // integer ending position (note y is the horizontal axis in display function)
PROBLEM_LEVEL 1,2,3,4,or 5 // choose this number to yield the solution to the respective part of the problem (they aredescribed below)
FNAME "some_string_filename_here" // make this point to the path of a text file representing the map you want to solve must be a square of SIZE by SIZE
SIZE positive_integer // choose this as the side length of the square map specified in FNAME
ANIMATE 0,1 // choose 1 to display intermediate map state
ANIMATE_SKIP positive_integer n // show the display only every n iterations (n=1 shows every iteration)

compile and link the c++ code (all .cpp files are necessary)

run the executable. There are two optional command line parameters used only for PROBLEM_LEVEL 5
-the first parameter, an integer x, allows you to stop the solver prematurely when a path to the goal has been found that steps on at least x squares, set this parameter to -1 (its default value) to perform an exhaustive search (subject to memory limitations) the best found path will be found and written to file
-the second parameter,  an integer w, is a heuristic weighting value that controls the relative weight of the two heuristics. Best results (for the default heuristic choices) are obtained when setting this parameter to 1 or 2. the default is 2

sample compile and run commands (Windows)

// compile and run problem 5, using 1:1 heuristic weighting
g++ -c *.cpp -D PROBLEM_LEVEL=5
g++ -o main.exe *.o
main -1 1

// compile and run problem 5 using 2:1 heuristic weighting
g++ -c *.cpp -D PROBLEM_LEVEL=5
g++ -o main.exe *.o
main -1 2

// compile and run the problem 4 solver
g++ -c *.cpp -D PROBLEM_LEVEL=4
g++ -o main.exe *.o
main

Problem description
-------------------
[Knight Board]
The knight board can be represented in x,y coordinates. The upper left position
is (0,0) and the bottom right is (7,7). Assume there is a single knight chess
piece on the board that can move according to chess rules. Sample S[tart] and
E[nd] points are shown below:
. . . . . . . .
. . . . . . . .
. S . . . . . .
. . . . . . . .
. . . . . E . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
Level 1: Write a function that accepts a sequence of moves and reports
whether the sequence contains only valid knight moves. It should also
optionally print the state of the knight board to the terminal as shown
above after each move. The current position should be marked with a 'K'.
Level 2: Compute a valid sequence of moves from a given start point to a given
end point.
Level 3: Compute a valid sequence of moves from a given start point to a
given end point in the fewest number of moves.
Level 4: Now repeat the Level 3 task for this 32x32 board. Also, modify
your validator from Level 1 to check your solutions. This board has the
following additional rules:
1) W[ater] squares count as two moves when a piece lands there
2) R[ock] squares cannot be used
3) B[arrier] squares cannot be used AND cannot lie in the path
4) T[eleport] squares instantly move you from one T to the other in
the same move
5) L[ava] squares count as five moves when a piece lands there
. . . . . . . . B . . . L L L . . . . . . . . . . . . . . . . .
. . . . . . . . B . . . L L L . . . . . . . . . . . . . . . . .
. . . . . . . . B . . . L L L . . . L L L . . . . . . . . . . .
. . . . . . . . B . . . L L L . . L L L . . . R R . . . . . . .
. . . . . . . . B . . . L L L L L L L L . . . R R . . . . . . .
. . . . . . . . B . . . L L L L L L . . . . . . . . . . . . . .
. . . . . . . . B . . . . . . . . . . . . R R . . . . . . . . .
. . . . . . . . B B . . . . . . . . . . . R R . . . . . . . . .
. . . . . . . . W B B . . . . . . . . . . . . . . . . . . . . .
. . . R R . . . W W B B B B B B B B B B . . . . . . . . . . . .
. . . R R . . . W W . . . . . . . . . B . . . . . . . . . . . .
. . . . . . . . W W . . . . . . . . . B . . . . . . T . . . . .
. . . W W W W W W W . . . . . . . . . B . . . . . . . . . . . .
. . . W W W W W W W . . . . . . . . . B . . R R . . . . . . . .
. . . W W . . . . . . . . . . B B B B B . . R R . W W W W W W W
. . . W W . . . . . . . . . . B . . . . . . . . . W . . . . . .
W W W W . . . . . . . . . . . B . . . W W W W W W W . . . . . .
. . . W W W W W W W . . . . . B . . . . . . . . . . . . B B B B
. . . W W W W W W W . . . . . B B B . . . . . . . . . . B . . .
. . . W W W W W W W . . . . . . . B W W W W W W B B B B B . . .
. . . W W W W W W W . . . . . . . B W W W W W W B . . . . . . .
. . . . . . . . . . . B B B . . . . . . . . . . B B . . . . . .
. . . . . R R . . . . B . . . . . . . . . . . . . B . . . . . .
. . . . . R R . . . . B . . . . . . . . . . . . . B . T . . . .
. . . . . . . . . . . B . . . . . R R . . . . . . B . . . . . .
. . . . . . . . . . . B . . . . . R R . . . . . . . . . . . . .
. . . . . . . . . . . B . . . . . . . . . . R R . . . . . . . .
. . . . . . . . . . . B . . . . . . . . . . R R . . . . . . . .
Level 5 [HARD]: Compute the longest sequence of moves to complete Level 3 without
visiting the same square twice. Use the 32x32 board.