



#define MAX_ITERS 7777777	// max A* loop iteratios
#ifndef ALTERNATE
#define ALTERNATE 1			// set this flag to alternate the ordering of neighbors between 
#endif

#include "problem_setup.h"	// modify this file to choose which problem to solve, filename of the board configuration, and parameters such as start and goal locations
#include "board.h"
#include "node.h"
#include <cstdio>
#include <cstring>
#include <iostream>
#include <fstream>
using namespace std;


void pause();
bool check_path(int *all_x, int *all_y, int n);							// check a sequence for L-shape
void sequence_check( istream &in);										// read a sequence of moves form stream and check it
int score_path(Node *temp, int &depth);
int** make_astar_heuristic_grid();										// testing out this heuristic where I compute the shortest number of steps from every grid square to the goal
int** make_wall_heuristic_grid();
Node* solve_tour(Node *node, int n, int bounty, int heuristic_weight, int **grid);		// recursive knight's tour code
Node* solve_astar(int start_x, int start_y, bool verbose, Node **all_ptrs);				// sub-routine to compute A*, plans to goal from starting point. 
																						// the parameter all_ptrs is the top of a linked list of nodes to clean up.
																						// all the Node objects created are added to this list
bool compare_swap(Node **list, int i, int heuristic_weight, int **grid=NULL);			// compares list elements i and i-1, swaps and returns true if out of order, otherwise returns false
int main1();															// program code for PROBLEM_LEVEL 1
int main_astar();														// program code for PROBLEM_LEVEL 2, 3, and 4
int main_tour(int BOUNTY, int heuristic_weight);						// program code for PROBLEM_LEVEL 5
void cleanup(Node **all_ptrs);											// function that deletes every Node in a linked list (useful for cleaning up memory after running A*)

int deepest=0;

unsigned long long all_iter=0;
Node *best=NULL;
int bestcost=0;

int main(int argc, char **argv)
{
	if(PROBLEM_LEVEL==1)
		return main1();
	if(PROBLEM_LEVEL>1 && PROBLEM_LEVEL <=4)
		return main_astar();
	if(PROBLEM_LEVEL==5)
	{
		// the defaults
		int x = -1;
		int heuristic_weight = 2; // the heuristic weighting value (only used with sorting option OPT_WT)
								   // best results so far were achieved with value of 210		
		// command line arguments
		if(argc >= 2)
		{
			sscanf(argv[1],"%d",&x);
			
			if(argc >= 3)
				sscanf(argv[2],"%d",&heuristic_weight);
		}
		return main_tour(x, heuristic_weight);

	}
	return 0;
}

int main1()
{
#ifdef FNAMEP
	if(strcmp(FNAMEP,"@USER")!=0)	// if there is a file name provided, (rather than "@USER") read from the file
	{
		ifstream in(FNAMEP);
		sequence_check(in);
		in.close();
	}
	else
#endif								// no file name provided, instead get sequence from the user
	{
		cout << "Please supply the steps, one coordinate pair (separated by SPACE) per line\n";
		cout << "Terminate the sequence by entering -1.\n";
		sequence_check(cin);
	}
	
}


int** make_wall_heuristic_grid(){
	int **grid = new int*[SIZE];
	Node *n = new Node(START_X,START_Y);
	for(int i=0; i<SIZE; ++i)
	{
		grid[i]=new int[SIZE];
		for(int j=0; j<SIZE; ++j)
		{	
			for(int dist=0; dist<SIZE/2; ++dist)
			{
				if( i-dist < 0 || j-dist < 0 || i+dist >= SIZE || j+dist >= SIZE
					|| n->b->blocked(i-dist,j) || n->b->blocked(i+dist,j)
					|| n->b->blocked(i,j-dist) || n->b->blocked(i,j+dist) )
				{
					grid[i][j] = dist;
					break;
				}
			} 
			printf("% 2d",grid[i][j]);
		}
		//printf("Progress: %d/%d\n",i+1,SIZE);
		printf("\n");
	}
	delete [] n;
	return grid;
}

int** make_astar_heuristic_grid(){
	int **grid = new int*[SIZE];
	for(int i=0; i<SIZE; ++i)
	{
		grid[i]=new int[SIZE];
		for(int j=0; j<SIZE; ++j)
		{	
			Node **garbage = new Node*;
			*garbage=NULL;
			Node *temp=solve_astar(i,j,false, garbage);
			if(temp==NULL)
				grid[i][j]=1000;
			else
				grid[i][j]=temp->f;
			// free used memory
			cleanup(garbage);
			printf("% 3d",grid[i][j]);
		}
		//printf("Progress: %d/%d\n",i+1,SIZE);
		printf("\n");
	}
	return grid;
}
void cleanup(Node **all_ptrs)
{
	Node *temp1=*all_ptrs, *temp2;
	//cout << "Cleaning up...";
	while( temp1 != NULL)
	{
		temp2 = temp1->next;
		delete [] temp1;
		temp1 = temp2;
	}
	//cout << "Done!" << endl;
}
Node* solve_astar(int start_x, int start_y, bool verbose, Node **all_ptrs)
{
	/*********************************************/
	/****       Declare and Initialize        ****/
	/*********************************************/

	bool no_repeat[SIZE][SIZE]; // avoid needlessly expanding from the same position twice
	for(int i=0; i<SIZE; ++i)
	{
		for(int j=0; j<SIZE; ++j)
		{
			no_repeat[i][j] = false;
		}
	}

	Node *top = new Node(start_x, start_y);	// top of the A* search queue
	
	long t=0;								// iteration index
	Node *temp;

	if(verbose)
	{
		cout << "Initial Board Configuration" << endl;
		top->b->draw(start_x, start_y);
		cout << endl;

		pause();
	}
	/*********************************************/
	/****       Begin the A* main loop         ***/
	/*********************************************/
	while(!top->atGoal() && t++<MAX_ITERS)	// main algorithm loop
	{
		temp=top;						// grab the top node from the sorted queue

#if ANIMATE									// the printing stuff
		if(t%ANIMATE_SKIP==0 && verbose)
		{
			cout << "iter " << t << "\tf+h = " << top->f+top->h << endl;
			top->b->draw(start_x, start_y);
		}
#endif

	/*********************************************/
	/****  Remove top node from the queue     ****/
	/*********************************************/
		top=temp->next;
		if(top!=NULL)
			top->prev = NULL;

		if(*all_ptrs==NULL)			// add pointer temp to the list of pointers for cleanup
		{
			temp->next=NULL;
			*all_ptrs=temp;
		}
		else{
			temp->next = *all_ptrs;
			*all_ptrs=temp;
		}
		
#ifndef LONG_WAY
		if(no_repeat[temp->x][temp->y]) 	// don't expand from the same spot twice
			continue;						// in a cluttered environment, or if rules change
											// this check may no longer be logically sound
		no_repeat[temp->x][temp->y]=true;
#endif
		// expand by generating all traversible neighbors and add them into the sorted queue
	/*********************************************/
	/****  Expand the node by generating all  ****/
	/****  traversible neighbors. Insert each ****/
	/****  into the sorted queue.             ****/
	/*********************************************/
		for(int k=0; k<NUM_NEIGHBORS; ++k)
		{
			int i=k;
#if ALTERNATE
			if(t%2==0) // optionally alternate the sampling order, so that one direction isn't always preferred by the algorithm
				i = NUM_NEIGHBORS-k-1;
#endif
				
			int x = temp->x + neighbor_x[i];	// coordinates of the child node
			int y = temp->y + neighbor_y[i];
			
			if(	temp->b->can_move(temp->x,temp->y,i) )// traversible if these conditions are true
			{
				Node *child = new Node(temp, x, y, &top); // the constructor places Node into the sorted linked list
			}
		}

		if(top==NULL)					// nothing's left in queue
		{
			return NULL;
		}

	} // end of A* loop
	if(verbose)
	{
		cout<< "Final Solution" << endl;
		top->b->draw(START_X, START_Y);
		cout << "Done!\nSteps Required: " << top->f << "\nA* iterations: " << t << endl;
	}
	////////////////////////////////////////////////////////////
	// add all pointers in A* queue to linked list for cleanup
	temp=top;
	while(temp->next!=NULL) temp=temp->next;
	temp->next = *all_ptrs;
	*all_ptrs=top;
	////////////////////////////////////////////////////////////
	
	return top;
}

int main_tour(int BOUNTY, int heuristic_weight) // knight's tour implementation
{
	if(START_X >= SIZE || START_Y >= SIZE || START_X <0 || START_Y <0)
	{
		cout << "Starting Coordinates ("<< START_X << ", " << START_Y <<") are out of bounds. Cannot step there. Quitting.\n";
		return 0;
	}
	
	cout << "Problem Level "<< PROBLEM_LEVEL << "... Challenge Accepted!\n";

	/*********************************************/
	/****       Declare and Initialize        ****/
	/*********************************************/

	int **h_grid=NULL;
#if (SORT_OPTION==OPT_WT_A || SORT_OPTION==OPT_W_A)
	h_grid=make_astar_heuristic_grid();	
#elif (SORT_OPTION==OPT_WT_W || SORT_OPTION==OPT_W_W)
	h_grid=make_wall_heuristic_grid();
#endif
	Node *top = new Node(START_X, START_Y);	// top of the A* search queue
	long t=0;								// iteration index
	Node *temp;

	cout << "Initial Board Configuration" << endl;
	Board *b_initial=top->b;
	b_initial->draw(START_X, START_Y);
	cout << endl;


	int bounty=0;
	int MAX_=0;
	
	if(BOUNTY>0)
	{
		bounty=BOUNTY;
	}
	else
	{
		for(int i=0; i<SIZE; ++i)
		{
			for(int j=0; j<SIZE; ++j)
			{
				if(b_initial->get(i,j) >=0)
				{
					 ++bounty;
					 MAX_+=b_initial->get(i,j);
					 if(b_initial->get(i,j)==0) MAX_++;
				}
			}
		}
	}
	cout << "Bound on steps = " << MAX_ << endl;
	cout << sizeof(Board) << " " << BYTES << endl;
	pause();
	Node *result = solve_tour(top,0,bounty, heuristic_weight, h_grid);
	if(result != NULL)
	{
		cout<< "Final Solution" << endl;
		result->b->draw(START_X, START_Y);

	/*********************************************/
	/****       Post processing               ****/
	/*********************************************/
		int depth;
		int cost;
		Node *temp=result;
		cost=score_path(temp, depth);
		int all_x[depth];
		int all_y[depth];
		temp=result;
		for(int i=depth-1; i>=0; --i) // loop to place the resulting path into x and y arrays
		{
			all_x[i] = temp->x;
			all_y[i] = temp->y;
			temp=temp->parent;
		}

	/*********************************************/
	/****       Write results to a file       ****/
	/*********************************************/
		char out_file[200];
		sprintf(out_file, "result_%d_moves_%d_steps.txt", depth, cost);
		ofstream out(out_file);
		if(out)
		{
			for(int i=0; i<depth; ++i)
			{
				out << all_x[i] << ' ' << all_y[i] << '\n';
			}
			out << -1 << '\n';
		}
	}
	else
	{
		cout << "Processed all paths. None found.\n";
	}
	return 0;
}

int score_path(Node *temp, int &depth)
{
	depth=1;
	int cost=0;
	while(temp->parent != NULL) // loop to count the number of moves
	{
		++depth;
		if(temp->b->get(temp->x,temp->y)==0)
			cost++;	// teleporters have a cost of 1, despite their code being 0
		else
			cost+=temp->b->get(temp->x,temp->y); // all other traversible squares have a code equal to their cost
		temp=temp->parent;
	}
	return cost;
}

int main_astar()			// my implementation of the A* algorithm, used for PROBLEM LEVEL 2-4
{
	if(START_X >= SIZE || START_Y >= SIZE || START_X <0 || START_Y <0)
	{
		cout << "Starting Coordinates ("<< START_X << ", " << START_Y <<") are out of bounds. Cannot step there. Quitting.\n";
		return 0;
	}
	
	cout << "Problem Level "<< PROBLEM_LEVEL << "... Challenge Accepted!\n";

	Node **garbage = new Node*;
	*garbage=NULL;
	Node *top = solve_astar(START_X, START_Y, true, garbage);

	if(top==NULL)
	{	
		cout<<"Search failed.\n";
		return -1;
	}

	//post processing
	int depth=1;
	Node *temp=top;
	while(temp->parent != NULL) // loop to count the number of moves
	{
		++depth;
		temp=temp->parent;
	}
	int all_x[depth];
	int all_y[depth];
	temp=top;
	for(int i=depth-1; i>=0; --i) // loop to place the resulting path into x and y arrays
	{
		all_x[i] = temp->x;
		all_y[i] = temp->y;
		temp=temp->parent;
	}
	cout << "Checking moves for validity...\n";
	check_path(all_x, all_y, depth);

	cleanup(garbage);

	return 0;
}

void pause()
{
	cout<<"<<<Press Enter to continue>>>";
	cin.ignore();
}

bool check_path(int *all_x, int *all_y, int n)
{			// Checks for valid knight moves
			// Only tests for an L-shaped displacement
	for(int i=1; i<n; ++i)
	{
		int dx=all_x[i]-all_x[i-1];
		int dy=all_y[i]-all_y[i-1];
		if( (abs(dx)==1&&abs(dy)==2)||(abs(dx)==2&&abs(dy)==1) )
			continue;
		printf("Invalid Knight Move sequence.\nFirst invalid step is from (%d,%d) to (%d,%d) on move %d.\n",
				all_x[i-1], all_y[i-1], all_x[i], all_y[i], i);
		return false;
	}
	printf("Valid Knight Move sequence.\n");
	return true;
}

void sequence_check( istream &in)
{			// reads the sequence of moves from the input stream (white space separated, terminated by -1)
			// performs the check on the sequence as required by PROBLEM_LEVEL 1
	if(!in)
	{
		cout << "File error. Quitting." << endl;
		return;
	}
	int size = 100;
	int *all_x = new int[size];
	int *all_y = new int[size];

	int n=0;
	
#if ANIMATE
	Board b;	// creates a Board object (default constructor reads from the file FNAME)
#endif
	in >> all_x[n];
	while(all_x[n]>=0)
	{
		in >> all_y[n++];
		// dynamic memory allocation to handle arbitrary input size
		if(n == size)
		{
			size+=100;
			int *temp_x = new int[size];	// make a bigger array
			int *temp_y = new int[size];
			for(int i=0; i<n; ++i)
			{
				temp_x[i] = all_x[i];		// copy array contents
				temp_y[i] = all_y[i];
			}
			delete [] all_x;				// delete the smaller array
			delete [] all_y;
			all_x = temp_x;
			all_y = temp_y;
		}
#if ANIMATE
		if(n%ANIMATE_SKIP==0)
		{
			cout << endl;
    		cin.ignore();
			if(! b.step(all_x[n-1], all_y[n-1]) )
				break;
			b.draw(START_X, START_Y);
		}
#endif
		in >> all_x[n];
	}
	cout << "Checking moves for validity...\n";
	check_path(all_x, all_y, n);
	delete [] all_x;
	delete [] all_y;
}

Node* solve_tour(Node *node, int n, int bounty, int heuristic_weight, int **grid)
{
	if(++all_iter>MAX_ITERS)
		return best;
		
	if(n>deepest)
	{
		deepest=n;
		printf("deepest = %d/%d\n",n,bounty);
	}
	//if(all_iter%1000==0)
	//	printf("AI %d\n",all_iter);
	if(node->atGoal())
	{
		if(n>=bounty)
			return node;
		int depth;
		int cost=score_path(node, depth);
		if(cost>=bestcost)
		{
			best = node;
			bestcost=cost;
		}
		return NULL;
	}
	if(!node->goal_reachable())
		return NULL;
		
	Node **list = new Node*[8];
	int num_moves=0;
	
	for(int i=0; i<NUM_NEIGHBORS; ++i)
	{
		int x = node->x + neighbor_x[i];	// coordinates of the child node
		int y = node->y + neighbor_y[i];
		
		if(	node->b->can_move(node->x,node->y,i) )// traversible if these conditions are true
		{
			list[num_moves++] = new Node(node, x, y, NULL, true); // the constructor places Node into the sorted linked list
		}
	}


	for(int i=1; i<num_moves; ++i)	// bubble sort the list of potential moves
	{								// sorting by h, then by f
		if(i<=0)
			continue;
		if(compare_swap(list, i, heuristic_weight, grid))
			i-=2;
	}
	
	for(int i=0; i<num_moves; ++i)
	{
		Node *temp = solve_tour(list[i],n+1,bounty, heuristic_weight, grid);
		if(temp!=NULL)
		{
			for(int j=0; j<num_moves; ++j)
			{
				if(j!=i)
					delete [] list[j];
			}
			return temp;
		}
	}
	return NULL;
}

bool compare_swap(Node **list, int i, int heuristic_weight, int **grid)
{
	int f1, f0, g1, g0, h1, h0; 
	if(grid!=NULL){
		g1=grid[list[i]->x][list[i]->y];
		g0=grid[list[i-1]->x][list[i-1]->y];
	}
	f1 = list[i]->f;
	f0 = list[i-1]->f;
	h1 = list[i]->h;
	h0 = list[i-1]->h;
	
#if SORT_OPTION==OPT_W_E
	if(
		h1 < h0							// sorting ascending by h
	  	 ||( (h1 == h0) && (f1 > f0) )	// then descending by f
	  	)
#elif SORT_OPTION==OPT_WT
	int a=h1*heuristic_weight - f1;		// sort by weighted sum
	int b=h0*heuristic_weight - f0;
	if( a < b )
#elif SORT_OPTION==OPT_E_W
	if(
		f1 > f0							// sorting descending by f
	  	 ||( (f1 == f0) && (h1 < h0) )	// then ascending by h
	  	)
#elif SORT_OPTION==OPT_W_A
	if(
		h1 < h0							// sorting ascending by h
	  	 ||(h1 == h0) && (g1 > g0)		// then descending by g
	  	)
#elif SORT_OPTION==OPT_WT_A
	int a=h1*heuristic_weight - g1;		// sort by weighted sum
	int b=h0*heuristic_weight - g0;
	if( a < b )
#elif SORT_OPTION==OPT_W_W
	if(
		g1 < g0							// sorting ascending by g
	  	 ||((g1 == g0) && (h1 < h0)	)	// then ascending by h
	  	 ||((g1 == g0) && (h1 == h0) && (g1 > g0))		// then descending by f
	  	)
#elif SORT_OPTION==OPT_WT_W
	int a=h1*heuristic_weight - g1;		// sort by weighted sum
	int b=h0*heuristic_weight - g0;
	if( a < b )
#endif
	{
		Node *temp=list[i];
		list[i] = list[i-1];
		list[i-1] = temp;
		return true;
	}
	return false;
}
